#!/usr/bin/env python

import logging
import gensim
import multiprocessing
import time
import json

from contextlib import contextmanager
from timeit import default_timer
from nltk.corpus import stopwords
from gensim import corpora, models, similarities
from gensim.corpora import Dictionary, HashDictionary, MmCorpus
from gensim.models import TfidfModel
from gensim import utils

@contextmanager
def elapsed_timer():
    start = default_timer()
    elapser = lambda: default_timer() - start
    yield lambda: elapser()
    end = default_timer()
    elapser = lambda: end-start


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

if __name__ == '__main__':
    filter_extremes = False
    lemmatize = True

    stop = stopwords.words('english')

    path = "./data/"
    files = []
    for i in range(0, 21):
        files.append(path + str(i) + '.txt')

    def tokenize(text):
        return [token.encode('utf8') for token in utils.tokenize(text, lower=True, errors='ignore') if 2 <= len(token) <= 30 and not token in stop]

    class GitHubCorpus(gensim.corpora.TextCorpus):
        def get_texts(self):
            for filename in self.input:
                yield tokenize(open(filename).read())


    gh_corpus = GitHubCorpus(files)

    if filter_extremes:
        gh_corpus.dictionary.filter_extremes()
        gh_corpus.dictionary.compactify()

    MmCorpus.serialize('gh_bow.mm', gh_corpus, progress_cnt=10000)
    gh_corpus.dictionary.save('gh_wordids.txt')

    mm = MmCorpus('gh_bow.mm')

    tfidf = TfidfModel(mm, id2word=gh_corpus.dictionary, normalize=True)
    tfidf.save('gh.tfidf_model')

    MmCorpus.serialize('gh_tfidf.mm', tfidf[mm], progress_cnt=10000)

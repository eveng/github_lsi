# GitHub LSI Analysis

See how effective LSI is for entity linking with GitHub data

## How to Run

- Run the scripts in this order:
    - `python preprocessing.py`
    - `python build_model.py`
    - `python analyze_results.py`

import gensim
from gensim.models.doc2vec import TaggedDocument
from collections import namedtuple
from gensim.models import Doc2Vec
import gensim.models.doc2vec
from collections import OrderedDict
import multiprocessing
# for timing
from contextlib import contextmanager
from timeit import default_timer
import time
import json

model = Doc2Vec.load("model_0")
doc_id = 0 # pick random doc, re-run cell for more examples
sims = model.docvecs.most_similar(doc_id, topn=3)  # get *all* similar documents

print(sims)

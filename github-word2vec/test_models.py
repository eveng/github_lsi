import gensim
from gensim.models.doc2vec import TaggedDocument
from collections import namedtuple
from gensim.models import Doc2Vec
import gensim.models.doc2vec
from collections import OrderedDict
import multiprocessing
# for timing
from contextlib import contextmanager
from timeit import default_timer
import time
import json
import random

path = "../bigger_sample_data/"
name_of_files = list()

result_model_0 = dict()
result_model_1 = dict()
result_model_2 = dict()
for i in range(0, 21):
    name_of_files.append(str(i)+".txt")
    result_model_0[i] = list() 
    result_model_1[i] = list() 
    result_model_2[i] = list() 

range_counter = list()
for name in name_of_files:
    i = 0
    with open(path+name, encoding="latin-1") as alldata:
        for line in alldata:
            i += 1
    range_counter.append(i)

print(range_counter)
print(sum(range_counter))

import pickle
with open("owners", 'rb') as f:
    owners = pickle.load(f)

model = Doc2Vec.load("model_0.model")
print(model.docvecs.count)
range_start = 0
range_end = 0
correct_ans = 0
incorrect_ans = 0
for idx, value in enumerate(range_counter):
    range_end += value
    #print("\nstart: " + str(range_start) + " End: " + str(range_end))
    for i in range(0, 30):
        doc_id = random.randint(int(range_start/20), int(range_end/20))
        #print(doc_id)
       # print(str(interested_index) + "\t", end="", flush=True)
        result_model_0[idx].append(i)
        result_model_0[idx][i] = list()
        sims = model.docvecs.most_similar(doc_id, topn=25)  # get top 3 similar documents
        #print(sims)
        #for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
        for index in range(0, len(sims)):
            if (sims[index][0]) > 3502:
                result_model_0[idx][i].append("incorrect")
                continue
            #print( str(label) + " " + str(index) + " " + owners[sims[index][0]] )
            if int(owners[(sims[index][0]*20)]) == int(idx):
                result_model_0[idx][i].append("correct")
                correct_ans += 1
            else:
                result_model_0[idx][i].append("incorrect")
                incorrect_ans += 1
    range_start += value 

print(correct_ans)
print(incorrect_ans)

model = Doc2Vec.load("model_1.model")
print(model.docvecs.count)
range_start = 0
range_end = 0
correct_ans = 0
incorrect_ans = 0
for idx, value in enumerate(range_counter):
    range_end += value
    #print("\nstart: " + str(range_start) + " End: " + str(range_end))
    for i in range(0, 30):
        doc_id = random.randint(int(range_start/20), int(range_end/20))
        #print(doc_id)
       # print(str(interested_index) + "\t", end="", flush=True)
        result_model_1[idx].append(i)
        result_model_1[idx][i] = list()
        sims = model.docvecs.most_similar(doc_id, topn=25)  # get top 3 similar documents
        #print(sims)
        #for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
        for index in range(0, len(sims)):
            if (sims[index][0]) > 3502:
                result_model_0[idx][i].append("incorrect")
                continue
            #print( str(label) + " " + str(index) + " " + owners[sims[index][0]] )
            if int(owners[(sims[index][0]*20)]) == int(idx):
                result_model_1[idx][i].append("correct")
                correct_ans += 1
            else:
                result_model_1[idx][i].append("incorrect")
                incorrect_ans += 1
    range_start += value 

print(correct_ans)
print(incorrect_ans)

model = Doc2Vec.load("model_2.model")
print(model.docvecs.count)
range_start = 0
range_end = 0
correct_ans = 0
incorrect_ans = 0
for idx, value in enumerate(range_counter):
    range_end += value
    #print("\nstart: " + str(range_start) + " End: " + str(range_end))
    for i in range(0, 30):
        doc_id = random.randint(int(range_start/20), int(range_end/20))
        #print(doc_id)
       # print(str(interested_index) + "\t", end="", flush=True)
        result_model_2[idx].append(i)
        result_model_2[idx][i] = list()
        sims = model.docvecs.most_similar(doc_id, topn=25)  # get top 3 similar documents
        #print(sims)
        #for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
        for index in range(0, len(sims)):
            if (sims[index][0]) > 3502:
                result_model_0[idx][i].append("incorrect")
                continue
            #print( str(label) + " " + str(index) + " " + owners[sims[index][0]] )
            if int(owners[(sims[index][0]*20)]) == int(idx):
                result_model_2[idx][i].append("correct")
                correct_ans += 1
            else:
                result_model_2[idx][i].append("incorrect")
                incorrect_ans += 1
    range_start += value 


print(correct_ans)
print(incorrect_ans)

import json
with open('top_25_result_model_0.json', 'w') as outfile:
	json.dump(result_model_0, outfile)
with open('top_25_result_model_1.json', 'w') as outfile:
	json.dump(result_model_1, outfile)
with open('top_25_result_model_2.json', 'w') as outfile:
	json.dump(result_model_2, outfile)


'''
doc_id = 0 # pick random doc, re-run cell for more examples
sims = model.docvecs.most_similar(doc_id, topn=3)  # get *all* similar documents

print(sims)


print(owners[126])

for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
    print(u'Owner: %s %s %s: <<  >>\n' % (owners[sims[index][0]], str(label), str(sims[index])))
'''

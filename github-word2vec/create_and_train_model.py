import gensim
from gensim.models.doc2vec import TaggedDocument
from collections import namedtuple
from gensim.models import Doc2Vec
import gensim.models.doc2vec
from collections import OrderedDict
import multiprocessing
# for timing
from contextlib import contextmanager
from timeit import default_timer
import time
import json

@contextmanager
def elapsed_timer():
    start = default_timer()
    elapser = lambda: default_timer() - start
    yield lambda: elapser()
    end = default_timer()
    elapser = lambda: end-start

### Loading data and structuring it ###
SampleDocument = namedtuple('SampleDocument', 'words tags owner')

path = "../bigger_sample_data/"
name_of_files = list()
for i in range(0, 21):
    name_of_files.append(str(i)+".txt")

alldocs = []  # will hold all docs in original order
owners = []
line_no = 0
count = 0
group = 20
i = 0
for name in name_of_files:
    with open(path+name, encoding="latin-1") as alldata:
        for line in alldata:
            tokens = gensim.utils.to_unicode(line).split()
            #tokens = line.split()
            words = tokens[:]
            tags = [line_no] # `tags = [tokens[0]]` would also work at extra memory cost
            owner = [str(i)]
            alldocs.append(SampleDocument(words, tags, owner))
            owners.append(str(i))
            if (count%group) == 0:
                line_no += 1
            count += 1
    i += 1

#print alldocs
doc_list = alldocs[:]  # for reshuffling per pass

### Building Model ###
cores = multiprocessing.cpu_count()
assert gensim.models.doc2vec.FAST_VERSION > -1, "this will be painfully slow otherwise"

simple_models = [
    # PV-DM w/concatenation - window=5 (both sides) approximates paper's 10-word total window size
    Doc2Vec(dm=1, dm_concat=1, size=800, window=2, negative=40, hs=0, min_count=2, workers=cores),
    # PV-DBOW
    Doc2Vec(dm=0, size=800, negative=40, hs=0, min_count=2, workers=cores),
    # PV-DM w/average
    Doc2Vec(dm=1, dm_mean=1, size=800, window=2, negative=40, hs=0, min_count=2, workers=cores),
]

# speed setup by sharing results of 1st model's vocabulary scan
simple_models[0].build_vocab(alldocs)  # PV-DM/concat requires one special NULL word so it serves as template
print(simple_models[0])
for model in simple_models[1:]:
    model.reset_from(simple_models[0])
    print(model)
    #model.save("bigger_sample.model")

models_by_name = OrderedDict((str(model), model) for model in simple_models)

import random
import numpy as np

from random import shuffle
import datetime

### Training model ###
alpha, min_alpha, passes = (0.025, 0.001, 5000)
alpha_delta = (alpha - min_alpha) / passes

print("START %s" % datetime.datetime.now())

for epoch in range(passes):
    shuffle(doc_list)  # shuffling gets best results

    for name, train_model in models_by_name.items():
        # train
        duration = 'na'
        train_model.alpha, train_model.min_alpha = alpha, alpha
        with elapsed_timer() as elapsed:
            train_model.train(doc_list)
            duration = '%.1f' % elapsed()

    print('completed pass %i at alpha %f' % (epoch + 1, alpha))
    alpha -= alpha_delta

print("END %s" % str(datetime.datetime.now()))

i=0
for name, model in models_by_name.items():
    model.save("model_"+str(i)+".model")
    i+=1

### Testing ###
#doc_id = np.random.randint(simple_models[0].docvecs.count)  # pick random doc, re-run cell for more examples
doc_id = 0 # pick random doc, re-run cell for more examples
model = random.choice(simple_models)  # and a random model
sims = model.docvecs.most_similar(doc_id, topn=3)  # get *all* similar documents
print(sims)
print(u'TARGET (%d) Owner (%s): << %s >>\n' % (doc_id, alldocs[doc_id].owner, ' '.join(alldocs[doc_id].words)))
print(u'SIMILAR/DISSIMILAR DOCS PER MODEL %s:\n' % model)
for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
    print(u'Owner: %s %s %s: << %s >>\n' % (alldocs[sims[index][0]].owner, label, sims[index], ' '.join(alldocs[sims[index][0]].words)))


import pickle
with open("owners", 'wb') as f:
    pickle.dump(owners, f)


'''

### Extensive Testing ###
result = dict()
for model in simple_models:
    result[str(model)] = dict()
    result[str(model)]['MOST'] = dict()
    result[str(model)]['MOST']['correct'] = 0
    result[str(model)]['MOST']['incorrect'] = 0
    result[str(model)]['MEDIAN'] = dict()
    result[str(model)]['MEDIAN']['correct'] = 0
    result[str(model)]['MEDIAN']['incorrect'] = 0
    result[str(model)]['LEAST'] = dict()
    result[str(model)]['LEAST']['correct'] = 0
    result[str(model)]['LEAST']['incorrect'] = 0
    ### For each model we do 1000 testing and see the result ###
    for i in range(0, 500):
        doc_id = np.random.randint(simple_models[0].docvecs.count)  # pick random doc, re-run cell for more examples
        sims   = model.docvecs.most_similar(doc_id, topn=model.docvecs.count)  # get *all* similar documents
        target_owner = alldocs[doc_id].owner
        #print(u'TARGET (%d) Owner (%s): << %s >>\n' % (doc_id, alldocs[doc_id].owner, ' '.join(alldocs[doc_id].words)))
        #print(u'SIMILAR/DISSIMILAR DOCS PER MODEL %s:\n' % model)
        for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
            #print(u'Owner: %s %s %s: << %s >>\n' % (alldocs[sims[index][0]].owner, label, sims[index], ' '.join(alldocs[sims[index][0]].words)))
            sim_owner = alldocs[sims[index][0]].owner
            if target_owner == sim_owner:
                result[str(model)][label]['correct'] += 1
            else:
                result[str(model)][label]['incorrect'] += 1

print result
with open('result_5.json', 'w') as outfile:
    json.dump(result, outfile)
'''

#!/usr/bin/env python

import logging
import gensim
import multiprocessing
import time
import json

from contextlib import contextmanager
from timeit import default_timer
from gensim import corpora, models, similarities
from gensim.corpora import Dictionary, HashDictionary, MmCorpus
from gensim.models import TfidfModel, LsiModel, LdaModel
from gensim import utils

@contextmanager
def elapsed_timer():
    start = default_timer()
    elapser = lambda: default_timer() - start
    yield lambda: elapser()
    end = default_timer()
    elapser = lambda: end-start


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

if __name__ == '__main__':

    use_tfidf = True
    use_lda = False

    logger = logging.getLogger(__name__)

    id2word = Dictionary.load('gh_wordids.txt')

    mm_bow = MmCorpus('gh_bow.mm')
    mm_tfidf = MmCorpus('gh_tfidf.mm')

    lsi = None
    lda = None
    if use_tfidf:
        logger.info(mm_tfidf)
        if use_lda:
            lda = LdaModel(corpus=mm_tfidf, id2word=id2word, num_topics=100, update_every=1, chunksize=10000, passes=1)
        else:
            lsi = LsiModel(corpus=mm_tfidf, id2word=id2word, num_topics=100)
    else:
        logger.info(mm_bow)
        if use_lda:
            lda = LdaModel(corpus=mm_tfidf, id2word=id2word, num_topics=100, update_every=1, chunksize=10000, passes=1)
        else:
            lsi = LsiModel(corpus=mm_bow, id2word=id2word, num_topics=100)

    if use_lda:
        lda.save('gh.ldamodel')
    else:
        lsi.save('gh.lsimodel')


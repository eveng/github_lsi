#!/usr/bin/env python

import logging
import gensim
import multiprocessing
import time
import json

from contextlib import contextmanager
from timeit import default_timer
from nltk.corpus import stopwords
from gensim import corpora, models, similarities
from gensim.corpora import Dictionary, HashDictionary, MmCorpus
from gensim.models import TfidfModel, LsiModel, LdaModel
from gensim import utils

@contextmanager
def elapsed_timer():
    start = default_timer()
    elapser = lambda: default_timer() - start
    yield lambda: elapser()
    end = default_timer()
    elapser = lambda: end-start


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

stop = stopwords.words('english')

def tokenize(text):
        return [token.encode('utf8') for token in utils.tokenize(text, lower=True, errors='ignore') if 2 <= len(token) <= 30 and not token in stop]


if __name__ == '__main__':

    use_tfidf = True
    use_lda = False

    logger = logging.getLogger(__name__)


    id2word = Dictionary.load('gh_wordids.txt')
    tfidf_mm = MmCorpus('gh_tfidf.mm')
    bow_mm = MmCorpus('gh_bow.mm')
    lsi = LsiModel.load('gh.lsimodel')
    if use_lda:
        lda = LdaModel.load('gh.ldamodel')
    print(lsi)

    # Get test data, corresponds to author 0
    query = None
    with open('./data/0_test.txt') as f:
        query = id2word.doc2bow(tokenize(f.read()))
    
    if use_tfidf:
        if use_lda:
            index = similarities.docsim.Similarity('gh_similarity.index', lda[tfidf_mm], num_features=27482)
        else:
            index = similarities.docsim.Similarity('gh_similarity.index', lsi[tfidf_mm], num_features=27482)
    else:
        if use_lda:
            index = similarities.docsim.Similarity('gh_similarity.index', lda[bow_mm], num_features=27482)
        else:
            index = similarities.docsim.Similarity('gh_similarity.index', lsi[bow_mm], num_features=27482)

    if use_lda:
        sims = index[lda[query]]
    else:
        sims = index[lsi[query]]

    print(sorted(enumerate(sims), key=lambda item: -item[1]))
    print('\n\n')

    for sims in index:
        print(sorted(enumerate(sims), key=lambda item: -item[1]))
